def define_env(env):
  @env.macro
  def apidoc(namespace, title="API Docs"):
    if namespace != "" and not namespace.endswith('.html'):
      namespace += ".html"
    return "<a href=\"%s%s\" target=\"_blank\">%s</a>" % (env.variables.apidocs, namespace, title)
  
  @env.macro
  def service(intf, avail, namespace):
    return """
| Detail | |
| -- | -- |
| **Interface** | `%s` |
| **Availability** | %s |
| **Docs** | %s |
""" % (intf, avail, apidoc(namespace, intf))
