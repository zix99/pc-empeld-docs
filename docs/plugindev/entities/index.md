# Entities

At the highest level, an `Entity` is an object (Not necessarily an in-world object) that is sync'd
between client and server.

Most of the time, when we talk about entities, we're referring to an in-game physical object with
a model and position, but that isn't always the case.

## Entity Types

...
