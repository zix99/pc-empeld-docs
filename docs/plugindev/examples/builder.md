# Builder Plugin

One of the most complete examples is the builder plugin.  It's a minecraft sandbox-style
mode that is completely open source.  Feel free to take a look at its source
code and use it as the basis for your own projects.

[You can find its code here](https://bitbucket.org/zix99/pc-builder)
