# Resources

!!! note
    These links are provided for your benefit only. No
    warranty or endorsement is implied.

## Textures & 2D

-   [CgTextures - Texture Resources](http://cgtextures.com)
-   [Gimp - Image Editor](http://www.gimp.org/)

## 3D/Models

-   [Blender - 3D Modeler](http://www.blender.org/)
