# Environment

Environment is the set of layers and orbitals that describe how the atmosphere around the player is presented.

See [creating environment](create-environment.md) to get started.
