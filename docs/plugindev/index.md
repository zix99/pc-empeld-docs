# Plugin Dev

!!! warn
    These docs are still under construction.  Feel free to contribute to the docs using the edit button on the top right of any page.

Welcome to the Plugin Developer's guide.  This wiki will cover everything from how to develop your first plugin, to a complete API reference of plugin-base.

If you'd like to help write these docs, please submit a MR to the [the git repo](https://bitbucket.org/zix99/pc-empeld-docs/src).

Considering using Empeld as your voxel game engine? Check out our [feature list](../features.md)!

!!! tip "Hosting"
    Looking for [Hosting a server](../game/hosting.md) instead?

## New to modding?

First, you'll need to set up an [environment](ide.md)

Next, I recommend you start with [Getting started with Boo Plugin](basic/creating-a-plugin-boo.md) or [Getting started with C#](basic/creating-a-plugin.md)

## What to Build?

Once you have your first project created and running, next it's time to decide what to build.  While Empeld
is game-focused, you can mix-and-match.  That means if you already have a favorite game-mod (Maybe CTF or Tower Defense)
that you can make your own world and use it with the game mod.

### Components

- [World](world/index.md)

## Examples

  - [Builder Mod](https://bitbucket.org/zix99/pc-builder) - Minecraft-style sandbox building mod

## Tutorials

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/blcACNjLihM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  * [Creating a Block](world/blocks/block.md)

## Reference

  * {{ apidoc('', 'API Documentation') }}
