# Noise generators

Here we will explore the
[Deterministic random numbers](determinism.md) generators
Empeld provides.

## Perlin

Frequency 512, Octaves 8

<img src="perlin.png" width="500" />

## Simplex

Frequency 512, Levels 8

<img src="simplex.png" width="500" />

## Cell Noise

<img src="cell.png" width="500" />

## Random

Pure random

<img src="random.jpg" width="500" />

## Noise

Noise with 0.25 likelihood.

<img src="noise.png" width="500" />
