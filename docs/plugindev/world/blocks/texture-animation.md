# Texture Animation

Texture animation in the world is achieved by using a a single texture,
with 64 frames (8x8), running over the course of 4 seconds (16 FPS). The
texture can be any square size (eg 2048x2048, 1024x1024, etc), as it
will be pre-processed before loaded into the game. The frames progress
first by going across the X axis, and then down the Y axis.

## Texture matrix

|     |     |     |     |     |     |     |     |
|-----|-----|-----|-----|-----|-----|-----|-----|
| 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   |
| 8   | 9   | 10  | 11  | 12  | 13  | 14  | 15  |
| 16  | 17  | 18  | 19  | 20  | 21  | 22  | 23  |
| 24  | 25  | 26  | 27  | 28  | 29  | 30  | 31  |
| 32  | 33  | 34  | 35  | 36  | 37  | 38  | 39  |
| 40  | 41  | 42  | 43  | 44  | 45  | 46  | 47  |
| 48  | 49  | 50  | 51  | 52  | 53  | 54  | 55  |
| 56  | 57  | 58  | 59  | 60  | 61  | 62  | 63  |

## Using in the block

In order to use, you must set the TextureResource to be the sequence you
must set two properties

``` csharp
        public override string TextureResource
        {
            get
            {
                return this.ResourceResolver.Resolve("seq/water.png");
            }
        }

        public override BlockAnimationMode AnimationMode
        {
            get
            {
                return BlockAnimationMode.TextureSpritesheet;
            }
        }
```

## Generating the texture

There are a few ways you can generate these textures. If you're so
inclined, you can always create them by hand via photoshop or gimp, but
I also wrote a tool you can use to generate them from frames found at
<https://bitbucket.org/zix99/pab>
