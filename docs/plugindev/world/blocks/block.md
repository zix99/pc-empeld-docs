# Block

A block is an individual representation of a Voxel, its appearance, and
physical properties. A group of blocks forms a [Chunk](../chunk.md). (Usually
16x16x16). Many chunks form the World.

Blocks implement the {{apidoc('interfacepluginbase_1_1_objects_1_1_world_1_1_blocks_1_1_i_block', 'IBlock')}} interface, usually via
`BlockBase`, or one of its successors (eg `EarthBase` or
`EssentialsBlock`)

## Creating a Block

To start off, create a new file, and name it of your block type. Then
decorate it with the `Block` attribute. Example:

Blocks require, at minimum, you define the `Name` and the
`TextureResource`. You may also have to inject the `IResourceResolver`
instance via [Plugin Dependencies](../../services/dependencies.md).

The `Block` Attribute tells [core](../../coremods/core.md) what the internal name of the
block is. This is how we identify it in save files and across the
network to the server.

``` csharp
[Block("SpecialDirt")]
public class DirtBlock
{
  public override string Name{
    get{ return "Special Dirt"; }
  }
  
  public override string TextureResource{
    get{ return this.ResourceResolver.Resolve("textures/mytexture.png"); }
  }
}

```
