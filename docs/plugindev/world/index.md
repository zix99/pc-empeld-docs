# World

The **world** are all the blocks you see around you.

At a high level, there are the following concepts:

- A [block](blocks/block.md) is an individual type of material in your world.  We all know these from minecraft
- A [Chunk](chunk.md) is a 16x16x16 group of blocks.  You mostly won't have to deal with chunks in Empeld, but it's good to be aware of them
- A [World](generator.md) is a near-infinite set of chunks

The easiest way to get started is by using the blocks in
[essentials.blocks](../coremods/essentials.blocks.md) and
writing your first [generator](generator.md)
