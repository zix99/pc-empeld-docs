# Deterministic Pseudo-Random Numbers

**Deterministic Pseudo-Random Numbers** (DPRN) is a core concept of
world generation in Empeld. This is due to how the world generator is
setup. Very simple, DPRN says that for a given value (e.g. X), the
output will always be the same. For example, we might create a Random3D
class in Empeld, and every time you pass in the value 1 it outputs 22
and ever time you input the value 0 it will output 100.

## Why is this Important?

Due to the way world generation works, it is necessary to be consistent
every time the world is generated. If the server restarts, or if a chunk
is regenerated, it expects to get the exact same values as they were the
last time they were generated, and the time they were generated before
that.

To help facilitate this, pluginbase comes with a number of helpful
classes:

-   Random3D - Deterministic random number generator
-   CellNoise - Deterministic cell noise generator
-   Simplex - Deterministic simplex noise generator
-   Perlin - Deterministic perlin noise generator

Each generator takes in a seed and outputs a value based on a
combination of the seed and the world-coordinate in either 2D or 3D.
They can also be operated on in simple fashion by doing standard image
operations on them (i.e. contract, gamma, add/sub/mult/divide).

For more on these, see [Noise generators](noise.md)
