# Block Cache

!!! warning
    Block cache is an advanced feature that may be deprecated at a future
    point in favor of generic and type-safe block lookups

The block cache is a mechanism in which we define a model class that
pre-loads block types so we don't need to look them up via gameplay. You
can do this manually, by initializing it in the constructor of the
World, but there's an easier way.

``` csharp
public class BlockCache
{
    [BlockCache(typeof(Air))]
    public readonly IBlock Empty;

    [BlockCache(typeof(Grass))]
    public readonly IBlock Grass;

    [BlockCache(typeof(Water))]
    public readonly IBlock Water;

    [BlockCache(typeof(Snow))]
    public readonly IBlock Snow;

    [BlockCache(typeof(Rock))]
    public readonly IBlock Rock;

    [BlockCache(typeof(Sand))]
    public readonly IBlock Sand;

    [BlockCache(typeof(BeachSand))]
    public readonly IBlock BeachSand;
}
```

``` csharp
...
public class MyWorld : GeneratorBase {

  private readonly BlockCache _cache;

  public MyWorld(int seed, IBlockLookup blockLookup){
    ...
    _myCache = blockLookup.PopulateCache<BlockCache>();
  }
  
  public override GetBlock(int x, int y, int z){
    if (z < 0) return _cache.Grass;
    return _cache.Empty;
  }
}
```
