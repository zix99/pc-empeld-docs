# Prefabs

Prefabs is a Volume of blocks meant to represented prefabricated areas
within the world. You can save bits of the world, re-load them later,
and include them with your mod.

They can also be used to clone sections of the world easily.

## Documentation

{{apidoc('namespaceessentials_1_1_prefabs', 'You can find documentation here')}}

## Format

In case you want to create/use an external program that takes advantage
of prefabs, here is the binary spec for the .pfb format:

### V1

| Name         | Type          | Bytes                          | Notes                                                                          |
|--------------|---------------|--------------------------------|--------------------------------------------------------------------------------|
| Magic Length | byte          | 1                              | The length of the magic header string (val: 6)                                 |
| Magic        | char          | n                              | The characters for the magic. Should always be "PREFAB"                        |
| Version      | byte          | 1                              | The version number. Currently 0                                                |
| Width        | signed int32  | 4                              |                                                                                |
| Length       | signed int32  | 4                              |                                                                                |
| Height       | signed int32  | 4                              |                                                                                |
| CenterX      | signed int32  | 4                              |                                                                                |
| CenterY      | signed int32  | 4                              |                                                                                |
| CenterZ      | signed int 32 | 4                              |                                                                                |
| Blocks\[\]   | ushort array  | 2 \* width \* length \* height | The array of blocks. Coordinate determined by: z\*width\*length + y\*width + x |
