# Chunk

A chunk is a grouping of blocks that is used to drive things like:
Saving, Rendering, Network Syncing, Audio, Particle Effects, Physics,
etc. We create this grouping to help optimize for performance.

A chunk is usually 16x16x16 [Blocks](blocks/block.md) but isn't required to be.

A plugin has no direct control, nor knowledge, of a chunk.
