# Essential Blocks

There are a few different block-packs that are included
in Emeld for you to use. This includes:

- `essentials` (More [here](essentials.md))
- `essentials.blocks.alien` {{ apidoc('namespaceessentials_1_1blocks_1_1alien.html') }}
- `essentials.blocks.structural` {{ apidoc('namespaceessentials_1_1blocks_1_1structural.html' )}}
- `essentials.blocks.vegetation` {{ apidoc('namespaceessentials_1_1blocks_1_1vegetation.html') }}