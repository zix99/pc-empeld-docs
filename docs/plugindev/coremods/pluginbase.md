# PluginBase

The pluginbase assembly is stores all common code and contracts between
a plugin and the main game engine. Things like dependencies, math
extensions, vector extensions, Perlin Noise (and other world helpers),
and base classes are in pluginbase. Pluginbase doesn't have any
functionality on its own.

All plugins are required to reference pluginbase. If you looking for
more common and helpful things please see [Essentials](essentials.md) and
[Action essentials](essentials.action.md)

{{apidoc('namespacepluginbase', 'Click here to see the API docs for pluginbase')}}

## Dependencies

One of the primary features of pluginbase is giving you access to
internal features, such as saved game, or the block manager.

### Sample Use

Simply put, anything marked with the \[Dependency\] attribute is added
when this.InjectDependencies() is called within the constructor. The
interfaces must be provided as a dependency in the core or from another
plugin.

``` csharp
public class TestSubsystem : SubsystemBase
{
    [Dependency]
    protected IWorld World{get; private set;}

    [Dependency]
    protected IBlockLookup Blocks{get; private set;}

    [Dependency]
    protected IEntityManager EntityManager{get; private set;}

    public TestSubsystem()
    {
        //Call this in the constructor to populate anything marked with [Dependency]
        this.InjectDependencies();
    }
    
    .....
}
```

### Available

-   **IWorld** - The current active world framework. Used to read/write
    blocks at coordinates

## Helpers

## Base Classes
