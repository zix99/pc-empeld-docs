# Essentials

The essentials plugin strives to provide a core set of "essential" things that modders can use to build off of for their plugins.  It provides the following:

  * A core set of block types you can use (Water, dirt, rock, etc)
  * Simple environments and orbital objects
  * Many other logical pieces and extensions