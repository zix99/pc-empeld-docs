# Empeld Core

**Empeld Core**, or just *Core*, is the main graphics and networking
engine behind the game. It's what loads, syncs, and renders all plugins
given to it.
