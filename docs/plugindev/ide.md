# Download an IDE

## What is an IDE?

An Integrated Development Environment (IDE) is an application that
provides tools, auto-completion, and compilation for one or more
languages. Plugins for Empeld are written primarily in C#, thus require
an IDE that supports the language. There are some areas that are written
in Python or GLSL, which can be done in a simple text editor (such as
notepad++ or sublime).

### Available IDEs

-   [Visual Studio Express
    Edition](http://www.microsoft.com/visualstudio/eng/downloads#d-2012-express)
-   [Monodevelop](http://monodevelop.com/)

## Fancy Text Editors

If you'd prefer to not use an IDE, or plan on writing your code in Boo, a less
feature-full (but often faster) coding application may be better suited.

### Options

- [Visual Studio Code](https://code.visualstudio.com/)
- [Sublime Text](https://www.sublimetext.com/)
