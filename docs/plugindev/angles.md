# Radians or Degrees?
This topic has been discussed to death quite frankly during the development of the game.  The general consensus is that we should use both, but define a clear line between two of them.  Here is the rules we decided and why we made the decision:

If you are unfamiliar with Radians, please read [this wikipedia article](http://en.wikipedia.org/wiki/Radian).

  - In-code, use Radians
    - This is because all the matrix methods, sin/cos, internal math operations/etc are all based on Radians.  This makes the most sense to store all internal values as Radians.
  - In-configuration with external files, use Degrees
    - Degrees, quite simply, are easier to read and more intuitive into what they represent.  Things like the [[Model XML]] format and other configurations will store values in Degrees.