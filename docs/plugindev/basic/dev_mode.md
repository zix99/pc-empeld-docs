# Dev mode

Dev mode, or developer mode, is a mode in Empeld that enables you to test changes more quickly.

It does so by enabling the following:

  * Start the game in a limited mode, without logging in to your account
  * Test the World and Environments in the game
  * Load faster, by not having to load the entire server and entities
  * When writing [Boo](boo.md), it will auto-detect changes and re-load itself

You can run in dev mode by specifying `--dev` upon start the game.  For full details, run:

```
empeld.exe -h
```

## Examples

### Starting a Full Game

This is useful to test a full mod to make sure it works with multiplayer

```sh
empeld.exe -a username:password \
  -bgserver -join=localhost \
  -plugins=essentials,essentials.action,essentials.blocks.structural,essentials.blocks.alien,essentials.blocks.vegetation,sandbox \
  --world=Sandbox --game=Sandbox --environment=Basic
```

### Testing a World

This tests a "World" without instantiating the game, server, or many of the effects.  Loads the game much faster. Supports hot-reloading if a binary/script changes.

```sh
empeld.exe -dev \
  -plugins=essentials,essentials.action,essentials.blocks.structural,essentials.blocks.alien,essentials.blocks.vegetation,sandbox \
  --world=Sandbox --environment=Basic
```

### Testing a Model

```sh
empeld.exe --dev-model path/to/my/model.mml
```

### Testing a Particle System

```sh
empeld.exe --dev-psys path/to/my/particlesystem.psys
```

### Testing UI

#### BUI (Boo UI)

```sh
empeld.exe --dev-ui path/to/my/ui.xml
```

#### UI2

```sh
empeld.exe --dev-ui2 path/to/my/ui2.xml
```