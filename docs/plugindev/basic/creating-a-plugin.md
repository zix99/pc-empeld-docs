# Creating a Plugin
This will go over the first steps of creating a plugin from scratch.

!!! tip "Getting started in Boo?"
    See: [Getting started with boo plugin](creating-a-plugin-boo.md)

### Starting the project
Starting is easy.  In the game folder, open up a terminal. On windows, you can do this by holding down shift, and right clicking in the window, then clicking "Open command line here".  Then, run the following command to start in dev-mode (replace `myplugin` with the name of the your plugin, and `WorldName` with the name of your world):

```
empeld.exe --dev --plugins essentials,myplugin --world WorldName
```

You can see a full list of arguments by running

```
empeld.exe -h
```

To see other ways to start empeld from CLI, see [Dev Mode](dev_mode.md)

## In CSharp

### Setting up a Project
  - Start by opening up your IDE and creating a new C# library project
  - Add a reference to *pluginbase.dll*, *essentials.dll*, and *OpenTK.dll* in the project. Make sure **copy-local** is False (or unchecked)
  - Add the following attribute line to your __AssemblyInfo.cs__ (Or equivalent file, or create the file if none exists)

```csharp
[assembly: EmpeldPlugin]
```

### Suggested Development
#### Setting up Running
It is recommended that you do a few additional steps while setting up your project to make it easier to work on.

  - Make the output path of your dll to be the *plugins* folder in the empeld install directory
  - Set the running application to be *empeld.exe*
    - Set the startup params to either:
      - `--dev`
      - `--plugins essentials,myplugin -a myusername:mypassword -bgserver -join localhost`
        - This will make sure your plugin is loaded, your logged in, and you're running in client-server mode
  - Create a 'content' folder next to your mode
    - Make sure to copy (or link) this content folder to your *plugins* folder, with the same name as your mod

You can see [Arguments](../..//game/arguments.md) for a full list of supported arguments.

### Distributing

When it comes time to share your plugin, you need to give out two pieces:

  - The compiled dll, eg `myplugin.dll`
  - The zipped resources of your content folder with the same name `myplugin.zip`

See [Distributing](distributing.md) for more detail.

## See Next
  * [Game Components](../components.md)
  * [World Generator](../world/index.md)
  * [Game Controller](../subsystems/index.md)
  * [Environment](../environment/index.md)