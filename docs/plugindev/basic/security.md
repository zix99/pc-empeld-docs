# Security
One of the most important topics around making mods for Empeld is security.  Since the language allows for anyone to make a mod, and for mods to do potentially harmful things to other computers, we make sure to reduce that risk as much as possible.

## How Signing Works
We use RSA signatures in Empeld to do a few different things:

  - To make sure when you update the game you're getting it from a valid source
  - To make sure plugin and game content on your computer is un-altered/hacked
  - Provide a way to validate your updates and make sure you have the most recent version

We do this by signing all the files, especially plugins, with our private key, and then verifying it everywhere with our public key.

You'll note that each mod and mod's content in the plugins folder has a .sig file with a hash in it. This is our signature, and Empeld knows that it can trust this.  By default, Empeld will warn the user about all non-signed content (Unless they have require_signature=false in their config file).  You can make life easier for your mod users by signing it yourself and giving them your public key to put in their trusted key store.

## Signing your Mods
You can sign your mods using our bundled RSA utility `security.exe`

This utility has a few commands:

  * create - To create a new private/public key pair
  * sign - To sign a file with your private key
  * verify - To verify a file/signature against your public key

Once you have created your private key with the util, and signed your content, you can distribute your public key for your users to drop in their "trustedkeys/" folder in empeld.  The key can also be installed by giving a URL to the launcher's "Install Key" feature under "Advanced".  This will mean that they trust all mods from you, and they won't be prompted every time they load up an unsigned server.