# Bocce

Bocce is the Empeld Boo internal preprocessor. It also ships next to the
game as `bocce.exe`, so that you can test compile your scripts outside
of the game.

Bocce is a preprocessor for the [Boo
language](https://github.com/bamboo/boo/wiki).

## Bocce preprocessor directives

### The import wildcard Bocce allows you to import nested namespaces
simply by specifying a wildcard. Eg. if you need to import a bunch of
namespaces in essentials, simply do:

     import essentials.Blocks.*

This can be very powerful, but will also generate a lot of unused
namespace warnings.

### Including other files You can include files from areas of your code,
or boccelib, simply by using the #include statement

     #include "macros"

This will automatically search within your project first, and then in
any included paths. For example, plugins includes boccelib/, and UI also
includes ui/lib/

### Assembly references

You can add an assembly reference to your file using

     #reference plugins/essentials.dll

### Macros Macros can be very powerful to simplify writing Boo code
(which can sometimes be verbose). They are similar to how C implements
macros, but combined with the power of regex.

eg. I define the @Dependency macro to be this:

     #define "@Dependency (\w+) (\w+)" "[pluginbase.Dependencies.Dependency] final $2 as $1"

#### Debugging macros Macros can be hard to debug, that's why I include
extra debug information when you compile using the \`-v\` flag
(Verbose).

### Special Variables Special variables in Bocce are surrounded by
`%`, and can be very useful in debugging or describing relative files
and paths.

Eg,

    @Declare TextureResource _resolver.Resolve("%DIR%/woodchips.jpg")

Here are defined special variables:

| Name       | Description                                             |
|------------|---------------------------------------------------------|
| %DIR%      | The current path to the source file path/to/            |
| %PATH%     | The full path to the source file, eg path/to/myfile.boo |
| %FILENAME% | Just the filename of the file, eg myfile.boo            |
| %LINE%     | The current line number                                 |
