# Distributing Mods

Up until this point, you've probably work in a mode where you have your
dll in the `plugins/` folder, right next to another folder for your
content. There's a very easy way to redistribute your mod as a single
bundle.

## C# DLL

### A single mod-zip (Preferred)

Let's say you have a plugin named `myplugin`. Create a new zip file
named `myplugin.zip` and put the following inside it:

-   Everything in your `content` folder
-   Your compiled dll renamed to `plugin.dll`

The result you you have 1 zip file you can sign and redistribute. Just
have you fans plop the entire zip file into their plugins folder (No
need to extract).

Make sure to read [Security and signatures](Security and signatures) so
you correctly sign your output file.

#### Why not to do this?

If you make a single mod-zip, that means that if people want to build
mods that extend your mod it'll be much harder for them since they won't
be able to reference your plugin. If you don't want this to happen, then
do it this way, otherwise, see the two-file method below.

### Separate dll and content zip

This deployment method keeps your dll and content separate. Let's say
(again) you have a mod named `myplugin`.

Your output will be two files:

1.  Your dll named `myplugin.dll`
2.  Your content zipped up named `myplugin.zip`

In this method you have to sign both files, and users have to put both
files in their `plugins/` folder. They don't need to unzip the zip
file.

---

## Distributing your Boo Plugin

There are several ways you can redistribute a Boo plugin.  Here are the two most common:

### Zip it up (Recommended)

The easiest way to distribute your boo plugin is to just create a zip file of your folder.  So if your folder name was `myplugin` you'd have `myplugin.zip`.  Then, you can just send this to your friends and have them drop it in their `plugins/` folder (No need to unzip it! Empeld can read zip files!)

### Compiling

If you'd like, you can also compile you're Boo code into a MSIL dll file, and distribute it the same way you distribute C# plugins. To compile, simply use the same params you do to test your plugin, but also specify an output via `-o`.

