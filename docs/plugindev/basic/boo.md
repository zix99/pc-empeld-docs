# Boo is a python-like language that is compiled by the CLR. This
makes it exception for Empeld, since it can pretty much run at
native-times.

You can read more about boo on [their github
wiki](https://github.com/bamboo/boo/wiki).

Read about Empeld's preprocessing: [Bocce](bocce.md)

## Performance tricks

In addition to the [Code rules for gamedev](Code rules for gamedev),
there are a few other tricks you should follow when writing in Boo.

### Use declared types for performance-critical pieces

By default, boo lets you use weak-typed variables to compute against.
eg:

```boo
def GetBlock(x, y, z):
      bla
```

This is nice to read, but because Boo can't tell what `x`, `y`, and
`z` are, it has to guess, and that can be slow. So it can be **MUCH**
faster if you specify it yourself.

```boo

def GetBlock(x as int, y as int, z as int) as IBlock:
     bla

```
