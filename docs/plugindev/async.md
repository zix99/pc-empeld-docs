# Async / Await

By default, you can use async/await in your plugins.  It's important to note that we override how it works from default to provide
more predictable behavior in empeld plugin development.

## How does it work?

The largest difference between what you're used to, is that when you `await` a Task in an `async` function that it is continued
on the main thread via the [ITaskScheduler](services/taskmanager.md) as a `QueueAsync`, which means it will be continued
as the soonest possible time.

Because it is synchronous, that means that anything you do needs to be as fast as you normally would keep it (it's not run in a different thread!).

There are some extensions in {{apidoc('classpluginbase_1_1_objects_1_1_tasks_1_1_task_ex', 'TaskEx')}}, on the TaskScheduler itself, and on various classes throughout empeld.
