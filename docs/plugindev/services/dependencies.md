# Dependencies

Dependencies are a way to get at various pieces of information from the
core game. Dependencies are often interfaces defined in pluginbase
(though not always) that other areas can provide to the plugin. There
are a few ways to get at these, but I'll describe the most common below.
These dependencies must be given the *Injectable* attribute.

## Injection

The best way to get at dependencies is to inject them into your class.
Lets say, for instance, you have a world generator, but you really need
to get at some information about the time (Which is stored in
[WorldState](WorldState)). You could do the following:

``` csharp
public class MyWorldGenerator : WorldGeneratorBase
{
  [Dependency]
  protected readonly IWorldState WorldState;
  
  public MyWorldGenerator(int seed, IBlockLookup blockLookup)
  {
    this.InjectDependencies(); //Fulfill all the dependencies in this class.
    ..whatever else you need to do..
  }
}
```

## Creating Dependencies

All dependencies must be marked with the *Injectable* attribute. These
can be anything (interfaces or classes), that you want to be able to
provide to other areas. To actually create an injectable, you need to
add it somewhere in code.

``` csharp
..
var myDependency = new SomeInjectable();
Game.Services.AddDependency(myDependency);
```

Alternatively, if a class can be self-instantiated, you can do something like this:

```csharp
[Injectable(AutoInstantiate = typeof(MyClass))] // Can be added to interface or class
internal class MyClass {
  protected MyClass() {
    // Empty constructor
    // Can be protected to prevent manual instatiation
  }
}
```

These created services are only available on the thread that they were
added in. This is done so that we can keep client and server
dependencies completely separated (Which shouldn't be a problem, since
you shouldn't really be using threads in your plugin anyway).
