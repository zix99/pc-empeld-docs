# Plugin Configuration

{{service('ISettings', 'Client and Server', 'interfacepluginbase_1_1_dependencies_1_1_i_settings')}}

There is a system in place that allows you to designate configurable values for a plugin or the component of a plugin.

Both of these methods use the `ConfigAttribute`.  It is either assembly-level or class-level.  If you put it on the assembly-level, if the plugin is included, the configurable item will show up.  If you put it on a component (World, Environment, or Game Controller), the config item will only show up if that component is used.

## Using the configuration

Let's say you have two config items.  A world waterlevel, and an assembly-wide setting about difficulty.

In your assemblyinfo, put something like this:
```csharp
[assembly: Config("difficulty", "Game difficulty", 1, Description="Sets the difficulty of the game")]
```

On your world, you might do something like:
```csharp
[Config("waterlevel", "Water Level", 10)]
[WorldGenerator(...)]
public class MyGenerator : WorldGeneratorBase {
...
}
```

To retrieve the value (whether set directly, or defaulting), you inject `ISettings`.  eg, back to the World generator case:
```csharp
public class MyGenerator : WorldGeneratorBase {
   [Dependency]
   private readonly ISettings _settings;
   
   private float _waterlevel;
   
   public MyGenerator(...) {
      this.InjectDependencies();
      _waterLevel = _settings.Get<int>("waterlevel");
   }
   
   ...
}
```

The same applies to the difficulty.

## Specifying settings on startup

For singleplayer, settings appear on the last screen of the New-Game configuration.

For multiplayer, there are two ways to introduce settings.
  - In your `empeld.config` file, add a `bag` entry for all settings. eg. `bag=waterlevel:30,difficulty:0`
  - When you start the game add a `--bag` parameter, and do the same thing
