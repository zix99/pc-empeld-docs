# Services

Services (or managers) are core or common elements that you can inject
into subsystems, components, or entities to access engine functionality.

For instance, you might inject `IReadonlyWorld` on the client to gain access
to blocks in the world.
