# empeld

## Bitmapbox

Region that displays a static image of a Bitmap

**Remarks:** BUI: bitmap

See Also: [Widget](#Widget), [Picturebox](#Picturebox)

#### Methods

<table style="width:98%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 13%" />
<col style="width: 9%" />
<col style="width: 51%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">Name</th>
<th style="text-align: left;">Params</th>
<th style="text-align: left;">Returns</th>
<th style="text-align: left;">Summary</th>
<th style="text-align: left;">See Also</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><strong>Clear</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Clear the image</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>Dispose</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Releases all resource used by the Bitmapbox object.</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>Draw</strong></td>
<td style="text-align: left;"><strong>slug</strong>:<br />
<strong>bitmap</strong>:</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Draw the specified bitmap using a cache</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>Draw</strong></td>
<td style="text-align: left;"><strong>bitmap</strong>:</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Draw the specified bitmap.</td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

#### Properties & Fields

| Name         | Type    | Summary                                                            | See Also |
|:-------------|:--------|:-------------------------------------------------------------------|:---------|
| **HasImage** | Boolean | Gets a value indicating whether this instance has an image defined |          |

## Button

Button to be clicked on

**Remarks:** BUI: Button

See Also: [WidgetFramed](#WidgetFramed)

#### Properties & Fields

| Name     | Type   | Summary                      | See Also |
|:---------|:-------|:-----------------------------|:---------|
| **Text** | String | Text displayed on the button |          |

#### Events

| Name                 | Type         | Summary                           | See Also |
|:---------------------|:-------------|:----------------------------------|:---------|
| **ButtonClickEvent** | OnClickEvent | Occurs when the button is clicked |          |

## Checkbox

Checkbox widget

**Remarks:** BUI: Checkbox

See Also: [WidgetFramed](#WidgetFramed)

#### Properties & Fields

| Name        | Type    | Summary                           | See Also |
|:------------|:--------|:----------------------------------|:---------|
| **Checked** | Boolean | Whether or not the box is checked |          |
| **Text**    | String  | Text displayed next to checkbox   |          |

## ClipMode

Widget clipping mode

#### Properties & Fields

| Name         | Type | Summary                             | See Also |
|:-------------|:-----|:------------------------------------|:---------|
| **Children** |      | Only children are clipped           |          |
| **Full**     |      | Children and the widget are clipped |          |
| **None**     |      | No clipping                         |          |

## Container

A container to hold and organize other widgets, that aligns

**Remarks:** BUI: Container, Block

See Also: [Widget](#Widget)

#### Properties & Fields

| Name              | Type                | Summary                            | See Also                                    |
|:------------------|:--------------------|:-----------------------------------|:--------------------------------------------|
| **FitToParent**   | Boolean             | Should we resize to fit our parent |                                             |
| **HAlign**        | HorizontalAlignment | Horizontal alignment of widgets    | [HorizontalAlignment](#HorizontalAlignment) |
| **PaddingBottom** | Int32               | Bottom side padding                |                                             |
| **PaddingLeft**   | Int32               | Left hand side padding             |                                             |
| **PaddingRight**  | Int32               | Right hand side padding            |                                             |
| **PaddingTop**    | Int32               | Top side padding                   |                                             |
| **VAlign**        | VerticalAlignment   | Vertical alignment of widgets      | [VerticalAlignment](#VerticalAlignment)     |

## Dialog

Dialog above screen

              Referred to "Window" in XML

**Remarks:** BUI: Dialog, Window

See Also: [Widget](#Widget)

#### Methods

| Name      | Params | Returns | Summary               | See Also |
|:----------|:-------|:--------|:----------------------|:---------|
| **Close** |        | Void    | Closes this instance. |          |

#### Properties & Fields

| Name            | Type              | Summary                                            | See Also                                |
|:----------------|:------------------|:---------------------------------------------------|:----------------------------------------|
| **Animated**    | Boolean           | Should it animate opening Default: True            |                                         |
| **HasClose**    | Boolean           | Does the dialog have a close button Default: True  |                                         |
| **HasTitle**    | Boolean           | Does the dialog have a title section Default: True |                                         |
| **MapPosition** | DialogMapPosition | Gets or sets the map position.                     | [DialogMapPosition](#DialogMapPosition) |
| **Title**       | String            | The title of the dialog                            |                                         |
| **TitleSize**   | Int32             | The height of the titlebar Default: 24             |                                         |

#### Events

| Name           | Type      | Summary                       | See Also |
|:---------------|:----------|:------------------------------|:---------|
| **CloseEvent** | Action\`1 | Occurs when the dialog closes |          |

## DialogMapPosition

Dialog screen position

#### Properties & Fields

| Name         | Type | Summary             | See Also |
|:-------------|:-----|:--------------------|:---------|
| **Absolute** |      | Absolute coordinate |          |
| **Center**   |      | Center of screen    |          |

## Dropbox

Simple container that receives a widget drop

**Remarks:** BUI: Dropbox

See Also: [WidgetFramed](#WidgetFramed)

## Frame

A frame for a single widget to live inside of.

              The framed widget is set to the same size as the frame.
              Adds a frame to a widget that might not have one (eg checkbox)

**Remarks:** BUI: Frame

See Also: [WidgetFramed](#WidgetFramed)

#### Properties & Fields

| Name               | Type    | Summary                                               | See Also |
|:-------------------|:--------|:------------------------------------------------------|:---------|
| **FitToParent**    | Boolean | Automatically resize to fit the parent Default: False |          |
| **ResizeChildren** | Boolean | Resize children to fit to the frame Default: True     |          |

## GridDropbox

A drop area that supports multiple items being dropped in a grid format

              Enumerable

**Remarks:** BUI: GridDropbox

See Also: [WidgetFramed](#WidgetFramed)

#### Methods

| Name              | Params   | Returns | Summary                                   | See Also |
|:------------------|:---------|:--------|:------------------------------------------|:---------|
| **RemoveControl** | **idx**: |         | Remove a widget in a specified slot index |          |

#### Properties & Fields

| Name           | Type    | Summary                          | See Also |
|:---------------|:--------|:---------------------------------|:---------|
| **DrawGrid**   | Boolean | Draw the grid? Default: True     |          |
| **SlotHeight** | Int32   | The height of an individual slot |          |
| **SlotsX**     | Int32   | Number of slots on the X axis    |          |
| **SlotsY**     | Int32   | Number of slots on the Y axis    |          |
| **SlotWidth**  | Int32   | The width of an individual slot  |          |

## Hidden

A hidden widget that isnt rendered

              Used to store values

**Remarks:** BUI: Hidden

#### Properties & Fields

| Name      | Type   | Summary            | See Also |
|:----------|:-------|:-------------------|:---------|
| **Value** | String | Value to be stored |          |

## HorizontalAlignment

Widget horizontal alignment.

#### Properties & Fields

| Name         | Type | Summary                                         | See Also |
|:-------------|:-----|:------------------------------------------------|:---------|
| **Absolute** |      | Absolute coordinate position                    |          |
| **Center**   |      | Center the position to the center of the parent |          |
| **Left**     |      | Align the position to the left of the parent    |          |
| **Right**    |      | Align the position to the right of the parent   |          |

## KeyCapturer

A widget that will capture a key pressed while it is activated

**Remarks:** BUI: KeyCapturer

See Also: [WidgetFramed](#WidgetFramed)

#### Properties & Fields

| Name    | Type | Summary                                          | See Also |
|:--------|:-----|:-------------------------------------------------|:---------|
| **Key** | Key  | The current key that is recognized by the widget |          |

## KeyValueTable

A simple key-value pair table of fixed size

**Remarks:** BUI: Undefined

See Also: [Widget](#Widget)

#### Properties & Fields

| Name            | Type          | Summary                                                    | See Also |
|:----------------|:--------------|:-----------------------------------------------------------|:---------|
| **ColumnWidth** | Single        | Gets or sets the width percentage of a column Default: 0.5 |          |
| **RowHeight**   | Int32         | Gets or sets the height of a row                           |          |
| **Rows**        | Dictionary\`2 | Rows of the table, a Dictionary, in Key-Value form         |          |
| **ShowBorder**  | Boolean       | Should we render the border                                |          |
| **ShowGrid**    | Boolean       | Should we render the grid                                  |          |

## Label

Simple label to display some text

**Remarks:** BUI: Label

See Also: [Widget](#Widget)

#### Properties & Fields

| Name              | Type    | Summary                                         | See Also |
|:------------------|:--------|:------------------------------------------------|:---------|
| **AllowOverflow** | Boolean | Allow text to overflow the width of the textbox |          |
| **Size**          | Int32   | Font size, in pixels Default: 18                |          |
| **Text**          | String  | Text                                            |          |

## Listbox\`1

A list of other child widgets, scrollable

              Enumerable

**Remarks:** BUI: Listbox

See Also: [WidgetFramed](#WidgetFramed)

#### Methods

<table style="width:99%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 19%" />
<col style="width: 7%" />
<col style="width: 49%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">Name</th>
<th style="text-align: left;">Params</th>
<th style="text-align: left;">Returns</th>
<th style="text-align: left;">Summary</th>
<th style="text-align: left;">See Also</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><strong>AddItem</strong></td>
<td style="text-align: left;"><strong>item</strong>:<br />
<strong>at</strong>:</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Adds an item to the list at a position</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>AddItem</strong></td>
<td style="text-align: left;"><strong>item</strong>:</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Add an item to the end of the list</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>Clear</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Clear the list</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>Move</strong></td>
<td style="text-align: left;"><strong>item</strong>: T<br />
<strong>count</strong>: Int32</td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Move an item in a direction</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>MoveDown</strong></td>
<td style="text-align: left;"><strong>item</strong>: T</td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Move item up in order of list</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>MoveUp</strong></td>
<td style="text-align: left;"><strong>item</strong>: T</td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Move item down in order of list</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>OnMouseDown</strong></td>
<td style="text-align: left;"><strong>x</strong>: Int32<br />
<strong>y</strong>: Int32<br />
<strong>button</strong>: MouseButton</td>
<td style="text-align: left;">Boolean</td>
<td style="text-align: left;">Mouse down event is special for a listbox for selection reasons</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>RemoveItem</strong></td>
<td style="text-align: left;"><strong>item</strong>: T</td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Remove an item from the list</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>RenderWidget</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Render!</td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

#### Properties & Fields

| Name                 | Type    | Summary                                                                   | See Also |
|:---------------------|:--------|:--------------------------------------------------------------------------|:---------|
| **ItemCount**        | Int32   | Count of items                                                            |          |
| **ScrollWheelPower** | Int32   | The power of the scrollwheel, in terms of how much it scrolls Default: 20 |          |
| **SelectedItem**     | T       | Gets or sets the current selected item                                    |          |
| **ShowSelectionBox** | Boolean | Show the selection box around the selected item                           |          |

#### Events

| Name                  | Type      | Summary                          | See Also |
|:----------------------|:----------|:---------------------------------|:---------|
| **ItemSelectedEvent** | Action\`1 | Occurs when an item is selected. |          |

## Numberbox

A special type of textbox for storing numbers

**Remarks:** BUI: NumberBox

See Also: [Textbox](#Textbox), [WidgetFramed](#WidgetFramed)

#### Properties & Fields

| Name              | Type  | Summary                                           | See Also |
|:------------------|:------|:--------------------------------------------------|:---------|
| **Increment**     | Int32 | How quick the smallest increment is               |          |
| **IncrementPage** | Int32 | How big a large increment is (page up, page down) |          |
| **MaxValue**      | Int32 | The maximum value of the number                   |          |
| **MinValue**      | Int32 | The minimum value of the number                   |          |
| **Value**         | Int32 | The current value                                 |          |

## OptionPicker

A widget that rotates through a set of options

**Remarks:** BUI: Option

See Also: [WidgetFramed](#WidgetFramed)

#### Methods

| Name         | Params | Returns | Summary                 | See Also |
|:-------------|:-------|:--------|:------------------------|:---------|
| **Next**     |        | Void    | Go to the next item     |          |
| **Previous** |        | Void    | Go to the previous item |          |

#### Properties & Fields

| Name             | Type       | Summary                                                                      | See Also |
|:-----------------|:-----------|:-----------------------------------------------------------------------------|:---------|
| **Options**      | String\[\] | Gets or sets the available set of options                                    |          |
| **OptionString** | String     | Gets or sets all the possible options, in string-deliminated form with a ',' |          |
| **Prefix**       | String     | Text prefix to display                                                       |          |
| **Value**        | String     | The current selected value, or "Undefined"                                   |          |

## Picturebox

Displays an image from a file

**Remarks:** BUI: Picturebox

See Also: [Widget](#Widget)

#### Properties & Fields

| Name           | Type    | Summary                                                               | See Also |
|:---------------|:--------|:----------------------------------------------------------------------|:---------|
| **AutoRedraw** | Boolean | Should picturebox auto redraw with the bitmap                         |          |
| **HasImage**   | Boolean | Gets a value indicating whether this instance has an active image.    |          |
| **Image**      | String  | Gets or sets the image. As a property so it can be used easily in BUI |          |

## ProgressBar

Progress bar

**Remarks:** BUI: Progressbar

See Also: [Widget](#Widget)

#### Properties & Fields

| Name       | Type   | Summary                                                                                   | See Also |
|:-----------|:-------|:------------------------------------------------------------------------------------------|:---------|
| **Border** | Int32  | The amount of space around the edge of the progress to show the background                |          |
| **Max**    | Int32  | Max Value                                                                                 |          |
| **Text**   | String | Gets or sets the format of the displayed inner text Special vars: {percent} {value} {max} |          |
| **Value**  | Int32  | Current value                                                                             |          |

## Scroller\`1

A scrollbox to animate scrolling between widgets

              Enumerable

**Remarks:** BUI: Scroller

See Also: [WidgetFramed](#WidgetFramed)

#### Methods

| Name           | Params        | Returns | Summary                                   | See Also |
|:---------------|:--------------|:--------|:------------------------------------------|:---------|
| **AddItem**    | **widget**: T | Void    | Add an item to display in the rotation    |          |
| **Clear**      |               | Void    | Remove all items from the rotation        |          |
| **RemoveItem** | **widget**: T | Void    | Remove an item to display in the rotation |          |

#### Properties & Fields

| Name             | Type           | Summary                                    | See Also |
|:-----------------|:---------------|:-------------------------------------------|:---------|
| **ArrowSize**    | Int32          | The size of the arrow                      |          |
| **InnerHeight**  | Int32          | Gets the inner widget height               |          |
| **InnerWidth**   | Int32          | Gets the inner widget width                |          |
| **ItemCount**    | Int32          | Get the count of the total number of items |          |
| **Items**        | IEnumerable\`1 | Enumerable for all items                   |          |
| **SelectedItem** | T              | Gets the current selected image            |          |

## Sprite

An animated picture

**Remarks:** BUI: Sprite

See Also: [Widget](#Widget)

#### Properties & Fields

| Name         | Type    | Summary                                 | See Also |
|:-------------|:--------|:----------------------------------------|:---------|
| **Animated** | Boolean | Should we automatically animate it      |          |
| **Frame**    | Int32   | The current frame                       |          |
| **FrameMs**  | Double  | How many milliseconds a frame lasts for |          |
| **Image**    | String  | The image to display, usually a gif     |          |

## Terminal

A terminal-like scrolling text box

**Remarks:** BUI: Terminal

See Also: [WidgetFramed](#WidgetFramed)

#### Methods

| Name          | Params              | Returns | Summary                      | See Also |
|:--------------|:--------------------|:--------|:-----------------------------|:---------|
| **WriteLine** | **content**: String | Void    | Write a line to the terminal |          |

#### Properties & Fields

| Name                    | Type | Summary                                                                                     | See Also |
|:------------------------|:-----|:--------------------------------------------------------------------------------------------|:---------|
| **FontSize**            |      | The font size, in pixels, of the lines                                                      |          |
| **MaxLines**            |      | Maximum number of lines to store                                                            |          |
| **MessageLifeSeconds**  |      | The number of seconds to display a message before expiring Default: 15                      |          |
| **ScrollY**             |      | The current scroll position                                                                 |          |
| **ShowExpiredMessages** |      | Should we render expired messages (Messages that are older than the lifespan) DEFAULT: True |          |

## Textbox

Textbox to accept user input or display a string

              Inherits: Widget

**Remarks:** BUI: Textbox

See Also: [WidgetFramed](#WidgetFramed)

#### Properties & Fields

| Name             | Type    | Summary                                                            | See Also |
|:-----------------|:--------|:-------------------------------------------------------------------|:---------|
| **Placeholder**  | String  | The placeholder text to display if the text field is empty or null |          |
| **ShiftPressed** | Boolean | Gets whether or not shift is pressed                               |          |
| **Text**         | String  | The contained text                                                 |          |

## VerticalAlignment

Widget vertical alignment.

#### Properties & Fields

| Name         | Type | Summary                                        | See Also |
|:-------------|:-----|:-----------------------------------------------|:---------|
| **Absolute** |      | Absolute coordinate position                   |          |
| **Bottom**   |      | Align the position to the bottom of the parent |          |
| **Middle**   |      | Align the position to the middle of the parent |          |
| **Top**      |      | Align the position to the top of the parent    |          |

## VerticalScrollbar

A vertical scrollbar, to scroll

**Remarks:** BUI: VerticalScrollbar

See Also: [WidgetFramed](#WidgetFramed)

#### Properties & Fields

| Name                 | Type  | Summary                                                    | See Also |
|:---------------------|:------|:-----------------------------------------------------------|:---------|
| **ButtonClickPower** | Int32 | How much clicking the up or down button affects the scroll |          |
| **ScrollMax**        | Int32 | The scroll max value                                       |          |
| **ScrollValue**      | Int32 | The current scroll value                                   |          |

## Widget

A base widget, which all other widgets are built on

#### Methods

<table style="width:99%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 15%" />
<col style="width: 6%" />
<col style="width: 54%" />
<col style="width: 7%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">Name</th>
<th style="text-align: left;">Params</th>
<th style="text-align: left;">Returns</th>
<th style="text-align: left;">Summary</th>
<th style="text-align: left;">See Also</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><strong>AddControl</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Add a child control to the widget</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>ClearControls</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Clears all the controls</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>Destroy</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Destroy the widget</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>Drop</strong></td>
<td style="text-align: left;"><strong>widget</strong>: Widget<br />
<strong>x</strong>: Int32<br />
<strong>y</strong>: Int32</td>
<td style="text-align: left;">Boolean</td>
<td style="text-align: left;">Drop the specified widget, x and y.<br />
Usually called by parent to drop widget that has been dragged</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>Find``1</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Find all children of a specific generic type</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>Find``1</strong></td>
<td style="text-align: left;"><strong>predicate</strong>:</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Find the specified predicate and given type</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>FindAll``1</strong></td>
<td style="text-align: left;"><strong>predicate</strong>:</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Finds all children of a given type and predicate, recursing into children</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>FindAll``1</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Finds all current children and sub children of type</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>Focus</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Event called when widget gains focus.</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>Hide</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Sets Visibility to False</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>LoseFocus</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Called when widget loses focus</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>RemoveControl</strong></td>
<td style="text-align: left;"><strong>widget</strong>: Widget</td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Remove a control inside us</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>SendAboveAll</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Sends widget, and all its parents, to the top of its respective siblings</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>SendBelowAll</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Sends widget, and all its parents, to the bottom of its respective siblings</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>SendToBottom</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Sends widget to the bottom of its siblings</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>SendToTop</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Sends widget to the top of its siblings</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><strong>Show</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Sets visibility to True</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><strong>StartDrag</strong></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">Void</td>
<td style="text-align: left;">Starts dragging the widget</td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

#### Properties & Fields

| Name                | Type             | Summary                                                                                                    | See Also |
|:--------------------|:-----------------|:-----------------------------------------------------------------------------------------------------------|:---------|
| **Attributes**      | Dictionary\`2    | Attributes about the widget that can store misc data. Used heavily in BUI\[glue\], but not specific to it. |          |
| **Clip**            | ClipMode         | Does the widget clip its inner contents Default: Children                                                  |          |
| **ClipOffset**      | Int32            | Offset the clipping to inner contents Default: 0                                                           |          |
| **Draggable**       | Boolean          | Is the widget draggable Default: False                                                                     |          |
| **DrawBounds**      |                  | Toggle drawing rulers (Globally)                                                                           |          |
| **Enabled**         | Boolean          | Is it enabled? (Clickable, Picks up signals, etc) Default: True                                            |          |
| **HasFocus**        | Boolean          | Does the widget have focus?                                                                                |          |
| **Height**          | Int32            | Gets or sets the height.                                                                                   |          |
| **Interactive**     | Boolean          | Can we click on it (or do clicks pass-thru) Default: True                                                  |          |
| **MouseOverWidget** | Widget           | The child widget the mouse is currently over                                                               |          |
| **Parent**          |                  | The parent widget                                                                                          |          |
| **Realized**        | Boolean          | Does it have a parent and is the parent realized. Not necessarily visible.                                 |          |
| **ScreenX**         | Int32            | Expensive property to get widget position from screen 0,0                                                  |          |
| **ScreenY**         | Int32            | Expensive function to get widget position from screen 0,0                                                  |          |
| **StyleClass**      | String           | The name of the currently active stylesheet class                                                          |          |
| **Styles**          | WidgetStyles     | The widget styles class object                                                                             |          |
| **Stylesheet**      | WidgetStylesheet | The widget stylesheet itself (eg the stylesheet)                                                           |          |
| **Tooltip**         | String           | The tooltip when the cursor hovers over the widget                                                         |          |
| **TooltipDelaySec** | Double           | The number of seconds to delay before showing the toolti\[ DEFAULT: 0.2                                    |          |
| **Visible**         | Boolean          | Is the widget visible? Default: True                                                                       |          |
| **Width**           | Int32            | Gets or sets the width.                                                                                    |          |
| **X**               | Int32            | X Coordinate of the widget Default: 0                                                                      |          |
| **Y**               | Int32            | Y Coordinate of the widget Default: 0                                                                      |          |
| **ZIndex**          | Int32            | Gets or sets the Z-Index of the widget, controlling its render order amongst its siblings                  |          |

#### Events

| Name                | Type            | Summary                                                 | See Also |
|:--------------------|:----------------|:--------------------------------------------------------|:---------|
| **DragEvent**       | Action          | Occurs when a drag starts                               |          |
| **DropDepart**      | Action\`1       | Occurs after a drop on the widget the item is departing |          |
| **DropEvent**       | Action          | Occurs when a drop occurs                               |          |
| **DropReceive**     | Action\`3       | Occurs on the receiving widget for the drop             |          |
| **KeyEvent**        | KeyboardEvent   | Raw keyboard input event on this                        |          |
| **MouseClickEvent** | MouseClickEvent | Mouse input event                                       |          |
| **MouseEnterEvent** | Action          | Occurs when mouse enter event.                          |          |
| **MouseLeaveEvent** | Action          | Occurs when mouse leave event.                          |          |
| **MouseMoveEvent**  | MouseMoveEvent  | Occurs when mouse move event.                           |          |
| **ResizeEvent**     | ResizeEvent     | Occurs when resize event.                               |          |

## WidgetFramed

A widget that has the default framing of a border and background

See Also: [Widget](#Widget)

#### Methods

| Name       | Params | Returns | Summary          | See Also |
|:-----------|:-------|:--------|:-----------------|:---------|
| **Redraw** |        | Void    | Redraw the frame |          |

#### Properties & Fields

| Name            | Type    | Summary                                                                                             | See Also |
|:----------------|:--------|:----------------------------------------------------------------------------------------------------|:---------|
| **IsActivated** | Boolean | Gets or sets a value indicating whether this instance is activated, triggering an appearance change |          |
| **IsHovering**  | Boolean | Gets a value indicating whether the mouse is hovering over the widget                               |          |

## WidgetStyles

Widget theme class.

#### Properties & Fields

| Name                        | Type    | Summary                          | See Also |
|:----------------------------|:--------|:---------------------------------|:---------|
| **ArrowImage**              | Texture | Arrow image for arrows           |          |
| **BackgroundActivated**     | IBrush  | Background activated color       |          |
| **BackgroundColor**         | IBrush  | The color of the background.     |          |
| **BackgroundHover**         | IBrush  | Background hover color           |          |
| **BackgroundImage**         | Texture | Background image                 |          |
| **BackgroundImageStretch**  | Single  | Background image stretch         |          |
| **BackgroundImageTileSize** | Int32   | Background tile size, if tiled   |          |
| **BackgroundSplit9**        | Boolean | Is background down using split-9 |          |
| **BackgroundTiled**         | Boolean | Is background tiled?             |          |
| **BorderColor**             | IBrush  | Border color                     |          |
| **BorderSize**              | Int32   | Border width                     |          |
| **ClickSound**              | ISample | Click sound                      |          |
| **Font**                    | GLFont  | The font name                    |          |
| **FontColor**               | Rgba    | Font color                       |          |
| **ForegroundActivated**     | IBrush  | The foreground activated.        |          |
| **ForegroundColor**         | IBrush  | The color of the foreground.     |          |
| **HasBackground**           | Boolean | Has background                   |          |
| **HasBackgroundHover**      | Boolean | Has background hover             |          |
| **HasBorder**               | Boolean | Does the widget have borders?    |          |
| **HasShadow**               | Boolean | Has shadow?                      |          |
| **ShadowColor**             | IBrush  | Shadow color                     |          |
| **ShadowImage**             | Texture | Shadow image                     |          |
| **ShadowImageTileSize**     | Int32   | Shadow image tile size           |          |
| **ShadowOffsetX**           | Int32   | Shadow offset X                  |          |
| **ShadowOffsetY**           | Int32   | Shadow offset Y                  |          |

Documentation generated by
[DocParser](https://bitbucket.org/zix99/docparser)
