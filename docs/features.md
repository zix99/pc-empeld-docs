# Feature List

## Main Features
  * Run on Windows, Linux, and Mac
  * Two rendering pipelines: Fixed, and GLSL (It will work on older cards and computers!)
  * Plugin friendly (Everything is a plugin)
    * Write plugins in either C# or Boo (Python-like)
  * Mix and match worlds, game modes, and environments from other plugins in both single player and multiplayer

## Components
  * Procedural world generation
  * Block filters (For seasonal changes and regeneration)
  * World modifies (Allows plugins to transparently alter other world generators to suit their needs)
  * Saving/Persist systems
  * Multiplayer synchronization system
  * RPC to send events to other clients or servers
  * Entity system that automatically synchronizes
  * Load animations, static objects
  * Bump maps, emissions, reflectivity (SERB) system
  * Script-based particle systems
  * World position based audio system
  * Near-infinite procedural world generation (2147483647 to -2147483647)

## UI Framework
  * XML Markup that defines 
  * Access and dynamically control UI with Boo
  * Use GlueStick to synchronize values from game objects with UI controls

## World Rendering features
  * Auto Level of Detail (LoD) for geometry in the world
  * Animated textures
  * Alpha/blending correctly
  * Shadows built-in
  * Reflection cube-map built-in
  * World is allowed to define gravity at different points

## Environment system
  * Environment consists of sky layers (Texture, colors, dynamic)
  * Orbital objects are images or textured spheres which have the ability to emit light
  * Orbitals correctly cast light on each other and on the world
  * Early models of wind that impacts particles and weather
  * Weather as a collision-based particle system

## Global rendering features
  * Shadows on everything
  * SSAO in post-processing pipeline
  * Allow custom post-processing filters (Both "plain" filters for older computers, and GLSL filters)

## Server
  * Server can run completely standalone and configured separately
  * Access server internals with a set of convenient JSON REST endpoints for things like
    * Internal server health
    * Resources
    * Online players

## Set of Essential Libraries
  * Several sets of "essential" libraries provide a foundation for game development
    * essentials - Base code for entities, simple environments, and prefab structures.  Also includes common things like in-game chat and scoreboard
    * essentials.action - The basics for FPS and gravity-based entities. Also implements item drops and pathing libraries, along with some basic AI classes
    * essentials.blocks.* - Various sets of common blocks, ranging from grass, rock, ice, water, to alien vegetation
      * essentials.blocks.vegetation
      * essentials.blocks.structural
      * essentials.blocks.alien
