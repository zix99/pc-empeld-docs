# Academic Use

## Why?
Empeld is the perfect game to learn intermediate to advanced object-oriented programming, in an industry relevant language (C#).  Students would exercise their skills in the language, in thinking in a 3 Dimensional environment, and in their own creativity.

## How?
If you wish to purchase more than five (5) copies of this game for academic use, send us an email (support@pontooncity.net) and discuss your situation.  We'll try to figure out an accommodating price on a case by case basis.
