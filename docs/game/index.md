# Game Documentation

This section holds documentation for running the game itself in complex ways.

This includes:

- Custom plugin setup
- Advance server features / Server API
- Dedicated server setup
