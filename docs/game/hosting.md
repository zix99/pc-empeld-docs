# Hosting a Server

## Networking

There are two ports that Empeld needs to run (by default, can be changed in config).  You
may need to do port-forwarding if you're self-hosting.

  * 43710 TCP & UDP (Game traffic)
  * 43711 TCP (Server metadata and joining information)

## Configuration

### Server config

All the configuration appears in the `empeld.default.config` file.  Simple copy the file to `empeld.config` to set any of the configuration.  This file is a way to specify any arguments that can be found by running `empeld.exe --help`.

The order of operations are:
  - Apply configuration defaults
  - Load `empeld.config` (if it exists)
  - Apply environment configuration, beginning with `EMPELD_` eg `EMPELD_PORT`
  - Apply any command line arguments (Overriding anything else)

### Administrators

To add a user as op (aka Administrator) simply add the username to `op.txt`

### Whitelist

Instead of controlling who can join with a password, you can allow or forbid individuals with a whitelist.  There are three ways to add or remove people from the whitelist.

To use whitelisting, it must be enabled in the server configuration.

  - In the server's console, type `whitelist USERNAME`
  - In the in-game chat (only if you're an administrator) type `/whitelist USERNAME`
  - Add a list of usernames (separated by newlines) to a `whitelist.txt` file in the same directory as the server
