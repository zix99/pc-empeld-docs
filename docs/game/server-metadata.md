# Server Metadata (HTTP Server)
Every game server has a built-in HTTP server that is used for the initial information gathering and inspecting of the server and its plugins before joining.  Below are the endpoints.  The default port for the server is 

## Endpoints

### /
Redirects to configured `server_homepage`.  Defaults to www.empeld.com

### /status

Returns the status of the server, including mods, players, and port the game is running on.
```json
{
  "Name": "Mega Awesome Test Server",
  "ActivePlayers": 1,
  "MaxPlayers": 32,
  "Players": [
    "zix99"
  ],
  "Mods": [
    "builder",
    "essentials.action",
    "essentials"
  ],
  "Seed": 4,
  "Website": "http://www.empeld.com",
  "Port": 43710,
  "Version": "0.1.301.1736"
}
```

### /players
Returns a list of players and stats.

```json
{
  "Players": [
    {
      "Name": "zix99",
      "Ping": 39,
      "ConnectionTime": 0.575800836
    }
  ]
}
```

### /resources

Returns a list of resources needed to join the server, their hash, and security signature.

```json
{
  "RootUrl": "/resources/",
  "Resources": [
    {
      "Filename": "builder.dll",
      "Hash": "vHc//AdAhCZH63OaUDvMoMHZMQE=",
      "Signature": "PX7gUynXZEmJEPb7Z8VzWefkAk42D5E4ciwd/9Zye/KugpKzCO+Z1Q==",
      "Size": 33792
    },
    {
      "Filename": "builder_content.zip",
      "Hash": "Ch8Eqx/4xCX9B6MbBSGOIE77NwA=",
      "Signature": "V8/6qK3SoKqZ16sGJ7607O1e2uVNyPzMagfzwlTpHrYaoJGBKHgWVw==",
      "Size": 224359
    },
    {
      "Filename": "essentials.action.dll",
      "Hash": "et4tegijZHnNZzqpA4DTXb3eBKo=",
      "Signature": "Pu/TBxRtPBR5RPZaPFs0RChdxA1X/AyASgOE7p8nIVgGYzqcpk3WKQ==",
      "Size": 75264
    },
    {
      "Filename": "essentials.action_content.zip",
      "Hash": "cwbBNLnIPw9c1f46sXp4nos9tKc=",
      "Signature": "UFVD3DlW08HmF+tggB3uzJAdogJEOXydyvn4mEqO56gsiQ8nZjCYJg==",
      "Size": 398823
    },
    {
      "Filename": "essentials.dll",
      "Hash": "x9LSea1SaJtt/fadXIjlg/pBw6E=",
      "Signature": "dgubMoMZq0577F+t39MVSqjl/rZ8HMrYfsKeYD47rX0YdmxszRTtZw==",
      "Size": 141312
    },
    {
      "Filename": "essentials_content.zip",
      "Hash": "1Vu40znBYu04NSfxIK4JiTdyvgI=",
      "Signature": "XjhbMUNFliLOxxY2G1S3skr2FwVYDOJqKU/axLOVxQJQ2D+40RefWg==",
      "Size": 79369119
    }
  ]
}
```

### /properties

Returns properties of the server the game was started with.

```json
{
  "Plugins": [
    "builder",
    "essentials.action",
    "essentials"
  ],
  "Seed": 4,
  "EnvironmentName": "Basic",
  "WorldGeneratorName": "Earth",
  "GameControllerName": "Builder",
  "PropBag": {
    "item_despawn": "900"
  }
}
```

### /inspect

Return internal server statistics.

```json
{
  "QueuedTasks": 5,
  "QueuedAsyncWork": 0,
  "SentBytes": 111053,
  "ReceivedBytes": 182744,
  "SentPackets": 3971,
  "ReceivedPackets": 2056,
  "Memory": 7819264,
  "UnmanagedMemory": 8192
}
```