# Arguments

Startup arguments can be passed to the application to tell it to do
certain things when it starts. These can be useful to help you quickly
debug or to start the application in different modes.

## Supported Arguments

``` bash
empeld.exe [args]
  Version: 0.1.5152.676
  Running: Unix 3.2.0.3

Arguments:
      --addr=VALUE           Server listen address (String [None])
      --audio-channels=VALUE Sets audio_channels (Int32 [256])
      --audio-device=VALUE   Sets audio_device (String [None])
      --audio-frequency=VALUE
                             Sets audio_frequency (Int32 [44100])
  -a, --auth, --login=VALUE  UNSECURE: Login with colon-separated credential-
                               s. i.e. username:password (String [None])
      --bag=VALUE            Properties passed to plugins (String [None])
      --bgserver             Runs server in background, also running client 
                               (Boolean [False])
      --clock-speed=VALUE    Sets clock_speed (Double [1])
  -d, --debug                Enable options to help debug plugins (Boolean 
                               [True])
      --dev                  Enable development mode (Boolean [False])
      --dev-environment      Enable environment rendering in dev mode 
                               (Boolean [True])
      --dev-world            Enable terrain rendering in dev mode (Boolean 
                               [True])
      --environment=VALUE    Controller to use for the environment (String 
                               [None])
      --game=VALUE           Controller to use for the game (String [None])
  -j, --join=VALUE           Join a server (String [None])
      --log-file-level=VALUE Set log verbosity level for file (Int32 [3])
      --log-filter=VALUE     Sets log_filter (String [])
      --log-level=VALUE      Set log verbosity level for console (Int32 [3])
      --max-users=VALUE      Sets max_users (Int32 [32])
      --plugins=VALUE        Ordered, Comma-separated plugin list (String [])
  -p, --port=VALUE           Server port (Int32 [43710])
      --report-errors=VALUE  Sets report_errors (ErrorReportMode [Ask])
      --require-signature    Require plugin signatures (Boolean [False])
      --save-frequency=VALUE How often the game should automatically save in 
                               seconds (Int32 [60])
      --seed=VALUE           World seed (Int32 [0])
  -l, --server               Server mode (Boolean [False])
  -r, --content-root=VALUE   Server content root URL (String [None])
      --homepage=VALUE       Server Homepage URL (String [None])
      --motd=VALUE           Message of the day (String [None])
  -n, --name=VALUE           Server name (String [Server])
      --server-save=VALUE    Name of the server save (String [server])
      --verify               Server verify content (Boolean [True])
  -s, --session=VALUE        Session token file (String [None])
  -h, -?, --help             Show help (Boolean [False])
      --stateport=VALUE      Server status port (Int32 [43711])
      --threads=VALUE        Sets threads (Int32 [0])
      --udp                  Allow server to use UDP (Boolean [True])
      --world=VALUE          Controller to use for the world (String [None])
```

For a full list of up-to-date arguments, please run:

    empeld.exe -h

## Useful Arguments

### Start and Immediately Join a Server

This can be useful when you want to test a plugin you're working on:

    empeld.exe -bgserver -join=localhost
