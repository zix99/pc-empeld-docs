# Welcome to Empeld Documentation

![Empeld](assets/logo.png)

This page holds the API and Plugin-Development documentation for [Empeld](https://www.empeld.com).  For
more information about empeld, or how to download, please see the website.
